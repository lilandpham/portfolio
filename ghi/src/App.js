import Navbar from "./components/Navbar";
import Home from "./components/Home";
import About from "./components/About";
import Skills from "./components/Skills";
import Work from "./components/Work";
import Contact from "./components/Contact";

function App() {
  return (
    <div>
      <Navbar />
      <Home />
      <About />
      <Skills />
      <div className="work-container">
        <Work/>
      </div>
      <div className="work-container">
        <Contact />
      </div>
    </div>
  );
}

export default App;
