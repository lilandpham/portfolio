import React from 'react'
import HTML from '../assets/html.png'
import CSS from '../assets/css.png'
import Python from '../assets/python.png'
import Javascript from '../assets/javascript.png'
import ReactLogo from '../assets/react.png'
import Mongo from '../assets/mongodb.png'
import Django from '../assets/django.png'
import GitLab from '../assets/gitlab.png'


function Skills () {
  return (
    <div name='skills' className='w-full h-screen bg-[#f9f5f0]'>
      {/* Container */}
      <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center items-center w-full h-full'>
        <div>
          <p className='text-4xl font-bold inline border-b-4'>Skills</p>
        </div>
        <p className='py-4 text-xl text-[#B4BAd4]'>These are the technologies I've worked with</p>

        <div className='w-full grid grid-cols-2 sm:grid-cols-4 gap-4 text-center py-8'>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={HTML} alt="HTML icon" />
            <p>HTML</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={CSS} alt="CSS icon" />
            <p>CSS</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={Python} alt="Python icon" />
            <p>PYTHON</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={Javascript} alt="Javascript icon" />
            <p>JAVASCRIPT</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={ReactLogo} alt="React icon" />
            <p>REACT</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={Mongo} alt="Mongo icon" />
            <p>MONGO DB</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={Django} alt="Django icon" />
            <p>DJANGO</p>
          </div>
          <div className='shadow-md shadow-[#c9b295] hover:scale-110 duration-500'>
            <img className='w-20 mx-auto' src={GitLab} alt="GitLab icon" />
            <p>GITLAB</p>
          </div>

        </div>
      </div>

    </div>
  )
}

export default Skills
