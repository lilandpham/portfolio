import React from 'react'
import {HiArrowNarrowRight} from 'react-icons/hi'
import Luna from '../assets/Luna.mp4'
import music from '../assets/MilesAway.mp3'
import { Link } from 'react-scroll';

function Home () {

    const playAudio = () => {
        const audio = document.getElementById("audio");
        audio.play();
    }

  return (
    <div name='home' className='w-full h-screen bg-[#f9f5f0]'>
        {/* Container */}
        <div className='max-w-[1000px] mx-auto px-3 flex flex-col justify-center h-full'>
            <div className='flex flex-col items-center'>
                <video style={{width:'400px', height:'400px'}} autoPlay loop muted className='w-full'>
                    <source src={Luna} type='video/mp4' />
                </video>
            </div>

            <div>
                <p>Hi, my name is</p>
                <h1 className='text-[#B4BAd4] text-3xl sm:text-5xl font-bold'>Liland Pham</h1>
                <h2 className='text-[#aba6a6] text-2xl sm:text-4xl font-bold'>I'm a Full Stack Developer.</h2>
                <p className='py-4 max-w-[700px]'>As a healthcare professional transitioning to a career as a software engineer, I bring a unique perspective and a passion for problem-solving. I am enthusiastic and determined, with a strong aptitude for tackling diverse challenges as a full-stack developer.</p>
            </div>
            <div className='flex gap-4'>
                <button className='border-2 px-6 py-3 my-2 items-center hover:bg-white' onClick={playAudio}>
                ↺ |◁   II   ▷|   ♡
                <br></br>
                    Play Music
                </button>
                <audio id="audio" loop autoPlay  src={music} type="audio/mpeg"></audio>
                <button className='group border-2 px-6 py-3 my-2 flex items-center hover:bg-white'>
                    <Link to='work' smooth={true} duration={500}>
                        View Work
                    </Link>
                    <span className='group-hover:rotate-90 duration-300'>
                        <HiArrowNarrowRight className='ml-3'/>
                    </span>
                </button>
            </div>
        </div>
    </div>
  )
}

export default Home
