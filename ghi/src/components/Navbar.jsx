import React, {useState} from 'react'
import {FaBars, FaTimes, FaGitlab, FaLinkedin} from 'react-icons/fa'
import {HiOutlineMail} from 'react-icons/hi'
import {BsFillPersonLinesFill} from 'react-icons/bs'
import Logo from '../assets/energy.png'
import { Link } from 'react-scroll';

function Navbar () {
    const [nav, setNav] = useState(false)
    const handleClick = () => setNav(!nav)

  return (
    <div className='fixed w-full h-[110px] flex justify-between items-center px-4 bg-[#e8ded1] text-white-300'>
        <div>
            <img src={Logo} alt="Liland logo" style={{width: '250px', paddingTop: '20px'}}></img>
        </div>

        {/* menu */}
            <ul className='hidden md:flex'>
                <li>
                    <Link to='home' smooth={true} duration={500}>
                        Home
                    </Link>
                </li>
                <li>
                    <Link to='about' smooth={true} duration={500}>
                        About
                    </Link>
                </li>
                <li>
                    <Link to='skills' smooth={true} duration={500}>
                        Skills
                    </Link>
                </li>
                <li>
                    <Link to='work' smooth={true} duration={500}>
                        Work
                    </Link>
                </li>
                <li>
                    <Link to='contact' smooth={true} duration={500}>
                        Contact
                    </Link>
                </li>
            </ul>

        {/* hamburger */}
        <div onClick={handleClick} className='md:hidden z-10'>
            {!nav ? <FaBars /> : <FaTimes />}
        </div>

        {/* Mobile menu */}
        <ul className={!nav ? 'hidden' : 'absolute top-0 left-0 w-full h-screen bg-[#e8ded1] flex flex-col justify-center items-center'}>
            <li className='py-6 text-2xl'>
            <Link onClick={handleClick} to='home' smooth={true} duration={500}>Home</Link>
            </li>
            <li className='py-6 text-2xl'>
            {' '}
            <Link onClick={handleClick} to='about' smooth={true} duration={500}>About</Link>
            </li>
            <li className='py-6 text-2xl'>
            {' '}
            <Link onClick={handleClick} to='skills' smooth={true} duration={500}>Skills</Link>
            </li>
            <li className='py-6 text-2xl'>
            {' '}
            <Link onClick={handleClick} to='work' smooth={true} duration={500}>Work</Link>
            </li>
            <li className='py-6 text-2xl'>
            {' '}
            <Link onClick={handleClick} to='contact' smooth={true} duration={500}>Contact</Link>
            </li>
        </ul>

        {/* Social Icons */}
        <div className='hidden lg:flex fixed flex-col top-[35%] left-0'>
            <ul>
                <li className='w-[160px] h-[60px] flex justify-between items-center ml-[-100px] hover:ml-[-10px] duration-300'>
                    <a className='flex justify-between items-center w-full' href="https://www.linkedin.com/in/lilandpham/">
                        LinkedIn <FaLinkedin size={30}/>
                    </a>
                </li>
                <li className='w-[160px] h-[60px] flex justify-between items-center ml-[-100px] hover:ml-[-10px] duration-300'>
                    <a className='flex justify-between items-center w-full' href="https://gitlab.com/phamliland">
                        GitLab <FaGitlab size={30}/>
                    </a>
                </li>
                <li className='w-[160px] h-[60px] flex justify-between items-center ml-[-100px] hover:ml-[-10px] duration-300'>
                    <a className='flex justify-between items-center w-full' href="mailto:phamliland@gmail.com">
                        Email <HiOutlineMail size={30}/>
                    </a>
                </li>
                <li className='w-[160px] h-[60px] flex justify-between items-center ml-[-100px] hover:ml-[-10px] duration-300'>
                    <a className='flex justify-between items-center w-full' href="https://docs.google.com/document/d/15_xuUdM9XvGPr1sjR-2w64eEC4RLek2KU6pAaf42buQ/edit?pli=1">
                        Resume <BsFillPersonLinesFill size={30}/>
                    </a>
                </li>
            </ul>
        </div>

    </div>
  )
}

export default Navbar
