import React from 'react';
import KittyCar from '../assets/Kittycar.mp4';
import Scrumptious from '../assets/Scrumptious.mp4';
import Clueless from '../assets/Clueless.mp4';
import '../index.css';

function Work () {
    return (
        <div name='work' className='w-full bg-[#f9f5f0] justify-center'>
            <div className='max-w-[1000px] mx-auto p-4 flex flex-col items-center'>
            <p className='text-4xl font-bold inline border-b-4'>Work</p>
            <div className='pb-8'>
                <p className='py-4 text-xl text-[#B4BAd4]'>Check out some of my recent work!</p>
            </div>

            <div className='grid grid-cols-1'>
            <div className='video-container'>
                <div>
                    <a href='https://gitlab.com/phamliland/kittycar' className='text-xl hover:text-[#9081da] hover:bg-[#f0f0f0]' src=''>KittyCar</a>
                    <video autoPlay loop muted className='video-element rounded-md'>
                    <source src={KittyCar} type='video/mp4' />
                    </video>
                </div>
                <br></br>
                <div>
                    <a href='https://gitlab.com/phamliland/recipe-web-pages' className='text-xl hover:text-[#9081da] hover:bg-[#f0f0f0]' src=''>Scrumptious</a>
                    <video autoPlay loop muted className='video-element rounded-md'>
                    <source src={Scrumptious} type='video/mp4' />
                    </video>
                </div>
                <br></br>
                <div>
                    <a  href='https://gitlab.com/XanderRubio/clueless' className='text-xl hover:text-[#9081da] hover:bg-[#f0f0f0]'>Clueless - I Wear Whatevr</a>
                    <video autoPlay loop muted className='video-element rounded-md'>
                        <source src={Clueless} type='video/mp4' />
                    </video>
                </div>
            </div>
            </div>
            </div>
        </div>
    );
};

export default Work;
