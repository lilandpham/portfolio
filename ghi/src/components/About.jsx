import React from 'react'

function About () {
  return (
    <div name='about' className='w-full h-screen bg-[#f9f5f0]'>
        <div className='flex flex-col justify-center items-center w-full h-full'>
            <div className='max-w-[1000px] w-full grid grid-cols-2 gap-8'>
                <div className='sm:text-right pb-8 pl-4'>
                    <p className='text-4xl font-bold inline border-b-4'>About</p>
                </div>
                <div></div>
                </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-3 gap-8 px-5'>
                <div className='sm:text-right text-2xl font-bold text-[#B4BAd4]'>
                    <p>
                        Hi! I am Liland, nice to meet you. Please take a look around.
                    </p>
                </div>
                <div>
                    <p>
                    A little bit about me: I am Vietnamese-American, born and raised in Houston, TX. &#128640;
                    My passions revolve around a few key interests: weightlifting, indulging in anime, and curating collections of adorable things. ⋆ ˚｡⋆୨୧⋆ ˚｡⋆✿ ⋆｡˚ ʚɞ
                    <br></br>
                    Welcome to my personal website, where I share the captivating facets that pique my curiosity and serve as sources of inspiration.
                    I am thrilled to embark on a journey of personal growth, both in my daily life and as a Software Developer.
                    </p>
                </div>
                <div>
                    <img style={{width:'250px', height:'auto'}} src='https://media3.giphy.com/media/q74A46mcYR55W1aRRV/giphy.gif' alt='hello kitty'/>
                </div>
            </div>
        </div>

    </div>
  )
}

export default About
