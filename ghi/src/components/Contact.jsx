import React from 'react'

function Contact () {
  return (
    <div name='contact' className='w-full h-screen bg-[#f9f5f0] flex justify-center items-center p-4'>
        <form method="POST" action="https://getform.io/f/a0a19657-3197-4fb1-b010-690684ab3237" className='flex flex-col max-w-[600px] w-full'>
            <div className='pb-8'>
                <p className='text-4xl font-bold inline border-b-4'>Contact</p>
                <p className='py-4 text-xl text-[#B4BAd4]'>Submit the form below or shoot me an email - phamliland@gmail.com</p>
            </div>
            <input className='bg-[#d7d4d4] p-2' type="text" placeholder='Name' name='name' />
            <input className='bg-[#d7d4d4] my-4 p-2' type="email" placeholder='Email' name='email' />
            <textarea className='bg-[#d7d4d4] p-2' name="message" rows='10' placeholder='Message'></textarea>
            <button className='border-2 hover:bg-white px-4 py-3 my-8 mx-auto flex items-center'>Let's Collaborate! &#10024; </button>
        </form>
    </div>
  )
}

export default Contact
